﻿using UnityEngine;

public class ShapeController : MonoBehaviour
{
    // Time before the shape fall once
    [SerializeField] private float _fallTime = 0.8f;
    [SerializeField] private float _increaseFallTimeAmount = 0.001f;
    
    // Store the last time that the shape fell down to know the next time we lower the shape
    private float _previousTimeTheShapeFell;
    private Camera _cam;
    // Use the closest wall from the camera to get the direction to move relativly from the camera
    private Transform _closestWallFromCamera;

    private void Start()
    {
        // Store highly used variables in advance for performance
        _cam = Camera.main;
        SpawnSystem.Instance.SpawnNewShape(transform);
    }

    private void Update()
    {
        SetClosestWallToTheCamera();
        Movement();
        Rotation();
        Falling();
        _fallTime -= _increaseFallTimeAmount * Time.deltaTime;
    }

    // Get the closest wall in order to move the shape relative to the view
    private void SetClosestWallToTheCamera()
    {
        float shortest = 1000000;
        for (int i = 0; i < GameManager.Instance._walls.Length; i++)
        {
            float deltaY = Vector3.Distance(_cam.transform.position, GameManager.Instance._walls[i].position);
            if (deltaY < shortest)
            {
                shortest = deltaY;
                _closestWallFromCamera = GameManager.Instance._walls[i];
            }
        }
    }

    private void Movement()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MoveInDirection(-_closestWallFromCamera.right);
        }

        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MoveInDirection(_closestWallFromCamera.right);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            MoveInDirection(_closestWallFromCamera.forward);
        }

        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MoveInDirection(-_closestWallFromCamera.forward);
        }
    }

    private void MoveInDirection(Vector3 direction)
    {
        transform.position += direction;
        if (!GridManager.Instance.ValidMove(transform))
        {
            transform.position -= direction;
        }
    }

    private void Rotation()
    {
        // Rotate on the Z Axes
        if (Input.GetKeyDown(KeyCode.Z))
        {
            RotateInDirectionByAngle(_closestWallFromCamera.forward, 90f);
        }
        // Rotate on the opposite of Z Axes
        else if (Input.GetKeyDown(KeyCode.A))
        {
            RotateInDirectionByAngle(-_closestWallFromCamera.forward, 90f);
        }
        // Rotate on the X Axes
        if (Input.GetKeyDown(KeyCode.X))
        {
            RotateInDirectionByAngle(_closestWallFromCamera.right, 90f);
        }
        // Rotate on the opposite of X Axes
        else if (Input.GetKeyDown(KeyCode.S))
        {
            RotateInDirectionByAngle(-_closestWallFromCamera.right, 90f);
        }
        // Rotate on the Y Axes
        if (Input.GetKeyDown(KeyCode.C))
        {
            RotateInDirectionByAngle(_closestWallFromCamera.up, 90f);
        }
        // Rotate on the opposite of Y Axes
        else if (Input.GetKeyDown(KeyCode.D))
        {
            RotateInDirectionByAngle(-_closestWallFromCamera.up, 90f);
        }
    }

    private void RotateInDirectionByAngle(Vector3 direction, float angle)
    {
        transform.Rotate(direction, angle);
        if (!GridManager.Instance.ValidMove(transform))
        {
            transform.Rotate(-direction, angle);
        }
    }

    private void Falling()
    {
        if (Time.time - _previousTimeTheShapeFell > (Input.GetKey(KeyCode.Space) ? _fallTime / 10 : _fallTime))
        {
            transform.position += Vector3.down;
            if (!GridManager.Instance.ValidMove(transform))
            {
                transform.position += Vector3.up;
                GridManager.Instance.ShapeArrived(transform);
            }
            _previousTimeTheShapeFell = Time.time;
        }
    }
}