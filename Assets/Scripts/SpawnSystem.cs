﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnSystem : MonoBehaviour
{
    // Singleton
    public static SpawnSystem Instance { get; private set; }

    [SerializeField] private Shape_ScriptableObject[] _shapes = null;
    [SerializeField] private GameObject _cubeObj = null;
    // How many cubes we want to assign the cubes pool to begin with
    [SerializeField] private int _howManyCubesToSaveInPool = 30;

    // Object Pooling
    private Queue<GameObject> _cubesPool = new Queue<GameObject>();
    // Save total odds once
    private int _totalOdds = 0;

    private void Awake()
    {
        // Singleton init
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
    }

    private void Start()
    {
        // Grow the pool the number of cubes we set to begin with
        for (int i = 0; i < _howManyCubesToSaveInPool; i++)
        {
            GrowCubesPool();
        }
        // Save total odds once
        foreach (Shape_ScriptableObject shape in _shapes)
        {
            _totalOdds += shape._appearanceOdds;
        }
    }

    private void GrowCubesPool()
    {
        GameObject instanceToAdd = Instantiate(_cubeObj);
        AddToCubesPool(instanceToAdd);
    }

    public void AddToCubesPool(GameObject objectToAdd)
    {
        objectToAdd.transform.parent = transform;
        objectToAdd.SetActive(false);
        _cubesPool.Enqueue(objectToAdd);
    }

    public void SpawnNewShape(Transform parent)
    {
        // Reset the shape position and rotation
        parent.position = transform.position;
        parent.rotation = transform.rotation;
        // Choosing and storing random shape
        Shape_ScriptableObject choosenShape = GetRandomShape();
        // The first cube local position is (0, 0, 0), And it is the pivot position
        Transform firstCube = AssignNewShape(choosenShape, parent);
        firstCube.localPosition = Vector3.zero;
        for (int i = 0; i < choosenShape._cubesPositions.Length; i++)
        {
            Transform childrenCube = AssignNewShape(choosenShape, parent);
            // Changing each cube local position
            childrenCube.localPosition = choosenShape._cubesPositions[i];
        }
    }

    // Returning a random shape
    private Shape_ScriptableObject GetRandomShape()
    {
        int randomRate = Random.Range(0, _totalOdds);
        int totalRatePass = 0;
        int i = 0;
        for (i = 0; i < _shapes.Length; i++)
        {
            totalRatePass += _shapes[i]._appearanceOdds;
            if (totalRatePass >= randomRate)
                break;
        }
        return _shapes[i];
    }

    private Transform AssignNewShape(Shape_ScriptableObject shape, Transform newParent)
    {
        // If the pool empty increase it
        if (_cubesPool.Count == 0)
        {
            GrowCubesPool();
        }
        GameObject instance = _cubesPool.Dequeue();
        instance.SetActive(true);
        // If have Renderer change the color
        if (instance.GetComponent<Renderer>())
        {
            MaterialPropertyBlock _propBlock = new MaterialPropertyBlock();
            Renderer _renderer = instance.GetComponent<Renderer>();
            // Get the current value of the material properties in the renderer
            _renderer.GetPropertyBlock(_propBlock);
            // Assign our new Color
            _propBlock.SetColor("_Color", shape._color);
            // Apply the edited Color to the renderer
            _renderer.SetPropertyBlock(_propBlock);
        }
        instance.transform.parent = newParent;
        return instance.transform;
    }
}