﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform _target = null;
    [SerializeField] private float _distance = 15f;
    [SerializeField] private float _xSpeed = 6f;
    [SerializeField] private float _ySpeed = 12f;
    [SerializeField] private float _yMinLimit = -20f;
    [SerializeField] private float _yMaxLimit = 89f;

    private float _rotationYAxis = 0f;
    private float _rotationXAxis = 0f;
    private Vector3 _negDistance;
    
    private void Start()
    {
        Vector3 angles = transform.eulerAngles;
        _rotationYAxis = angles.y;
        _rotationXAxis = angles.x;
        _negDistance = new Vector3(0f, 0f, -_distance);
        Vector3 newPosition = _negDistance + _target.position;
        transform.position = newPosition;
    }

    private void LateUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            _rotationYAxis += _ySpeed * Input.GetAxis("Mouse X");
            _rotationXAxis -= _xSpeed * Input.GetAxis("Mouse Y");
            // Limit the X axis rotation
            _rotationXAxis = Mathf.Clamp(_rotationXAxis, _yMinLimit, _yMaxLimit);
            Quaternion rotation = Quaternion.Euler(_rotationXAxis, _rotationYAxis, 0);
            Vector3 newPosition = rotation * _negDistance + _target.position;
            transform.rotation = rotation;
            transform.position = newPosition;
        }
    }
}