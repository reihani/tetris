﻿using UnityEngine;

[CreateAssetMenu(fileName ="New Shape", menuName ="Shape")]
public class Shape_ScriptableObject : ScriptableObject
{
    public Color _color = Color.white;
    public int _appearanceOdds = 10;
    [Header("   Ignore the first cube position as it always will be at position (0, 0, 0), And it is the pivot position. ")]
    public Vector3[] _cubesPositions;
}