﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Singleton
    public static GameManager Instance { get; private set; }

    public Transform[] _walls;

    [SerializeField] private Transform _boardParent = null;
    [SerializeField] private Transform _spawner = null;
    [SerializeField] private Transform _floor = null;
    [SerializeField] private int _height = 10;
    [SerializeField] private int _width = 7;
    [SerializeField] private int _depth = 7;

    private void Awake()
    {
        // Singleton init
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        CreateBoard();
    }

    private void Start()
    {
        GridManager.Instance.CreateGrid(_height, _width, _depth);
    }

    private void CreateBoard()
    {
        // Fill the spaces when the numbers are even / odds
        float depthAdd = _depth % 2 == 0 ? 0.5f : 0f;
        float widthAdd = _width % 2 == 0 ? 0.5f : 0f;
        float heightAdd = _height % 2 == 0 ? 0 : 0.5f;
        // Board settings
        _boardParent.position = new Vector3(_width / 2 - widthAdd, _height / 2 + heightAdd, _depth / 2 - depthAdd);
        // Spawner settings
        _spawner.position = new Vector3(_width / 2, _height, _depth / 2);
        // Floor settings
        _floor.localPosition = new Vector3(0f, _height / -2f - 0.5f);
        _floor.localScale = new Vector3(_width, 0.1f, _depth);
        // Walls settings
        // backward wall
        _walls[0].localPosition = new Vector3(0f, -0.55f, _depth / -2 - 0.55f + depthAdd);
        _walls[0].localRotation = Quaternion.Euler(Vector3.zero);
        _walls[0].localScale = new Vector3(_width, _height, 0.1f);
        // forward wall
        _walls[1].localPosition = new Vector3(0f, -0.55f, _depth / 2 + 0.55f - depthAdd);
        _walls[1].localRotation = Quaternion.Euler(0f, 180, 0f);
        _walls[1].localScale = new Vector3(_width, _height, 0.1f);
        // left wall
        _walls[2].localPosition = new Vector3(_width / -2 - 0.55f + widthAdd, -0.55f, 0f);
        _walls[2].localRotation = Quaternion.Euler(0f, 90, 0f);
        _walls[2].localScale = new Vector3(_depth, _height, 0.1f);
        // right wall
        _walls[3].localPosition = new Vector3(_width / 2 + 0.55f - widthAdd, -0.55f, 0f);
        _walls[3].localRotation = Quaternion.Euler(0f, -90, 0f);
        _walls[3].localScale = new Vector3(_depth, _height, 0.1f);
    }
}
