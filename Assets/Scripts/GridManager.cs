﻿using UnityEngine;

public class GridManager : MonoBehaviour
{
    // Singleton
    public static GridManager Instance { get; private set; }

    private int _height = 10;
    private int _width = 7;
    private int _depth = 7;
    // Cubes array of filled places
    private Transform[,,] _cubesGrid;

    private void Awake()
    {
        // Singleton init
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
    }

    public void CreateGrid(int height, int width, int depth)
    {
        _height = height;
        _width = width;
        _depth = depth;
        // I added 3 on the Height for extra 3 cubes height on spawn
        _cubesGrid = new Transform[_width, _height + 3, _depth];
    }

    // Check if the shape position is in a valid place
    public bool ValidMove(Transform shape)
    {
        foreach (Transform cube in shape)
        {
            int roundedX = Mathf.RoundToInt(cube.position.x);
            int roundedY = Mathf.RoundToInt(cube.position.y);
            int roundedZ = Mathf.RoundToInt(cube.position.z);
            // Check if one of the shape cubes is outside the board
            if (roundedX < 0 || roundedX >= _width || roundedY < 0 || roundedZ < 0 || roundedZ >= _depth)
            {
                return false;
            }
            // Check if one of the shape cubes is in a taken place
            if (_cubesGrid[roundedX, roundedY, roundedZ] != null)
            {
                return false;
            }
        }
        return true;
    }

    // After the shape finished his journey
    public void ShapeArrived(Transform shape)
    {
        AddToGrid(shape);
        CheckForClear();
        ReleaseShape(shape);
        SpawnSystem.Instance.SpawnNewShape(shape);
    }

    // Filled the array with the cubes positions
    private void AddToGrid(Transform shape)
    {
        foreach (Transform cube in shape)
        {
            int roundedX = Mathf.RoundToInt(cube.position.x);
            int roundedY = Mathf.RoundToInt(cube.position.y);
            int roundedZ = Mathf.RoundToInt(cube.position.z);
            _cubesGrid[roundedX, roundedY, roundedZ] = cube;
        }
    }

    /*
     * Check if there are any Squares all over the board,
     * Deleting any filled squares,
     * And moving the rows 1 step lower from above the deleted squares places
     */
    private void CheckForClear()
    {
        for (int y = _height - 1; y >= 0; y--)
        {
            if (HasSquare(y))
            {
                DeleteSquare(y);
                RowDown(y);
            }
        }
    }

    private bool HasSquare(int y)
    {
        for (int x = 0; x < _width; x++)
        {
            for (int z = 0; z < _depth; z++)
            {
                if (_cubesGrid[x, y, z] == null)
                    return false;
            }
        }
        return true;
    }

    private void DeleteSquare(int y)
    {
        for (int x = 0; x < _width; x++)
        {
            for (int z = 0; z < _depth; z++)
            {
                // Adding the cube back to the pool
                SpawnSystem.Instance.AddToCubesPool(_cubesGrid[x, y, z].gameObject);
                _cubesGrid[x, y, z] = null;
            }
        }
    }

    private void RowDown(int startingY)
    {
        for (int y = startingY; y < _height; y++)
        {
            for (int x = 0; x < _width; x++)
            {
                for (int z = 0; z < _depth; z++)
                {
                    if (_cubesGrid[x, y, z] != null)
                    {
                        _cubesGrid[x, y - 1, z] = _cubesGrid[x, y, z];
                        _cubesGrid[x, y, z] = null;
                        _cubesGrid[x, y - 1, z].position += Vector3.down;
                    }
                }
            }
        }
    }

    // Stop moving all the cubes in the shape
    private void ReleaseShape(Transform shape)
    {
        Transform[] allChildren = shape.GetComponentsInChildren<Transform>();
        foreach (Transform cube in allChildren)
        {
            // Skip the parent transform
            if (cube == shape)
            {
                continue;
            }
            cube.parent = SpawnSystem.Instance.transform;
        }
    }
}